#!/bin/sh

sed -Ei 's_(<prop oor:name="WarnAlienFormat" oor:type="xs:boolean" oor:nillable="false"><value>)true(</value></prop>)_\1false\2_g' /usr/lib/libreoffice/share/registry/main.xcd
sed -Ei 's_(<prop oor:name="MacroSecurityLevel" oor:type="xs:int" oor:nillable="false"><value>)2(</value></prop>)_\11\2_g' /usr/lib/libreoffice/share/registry/main.xcd
sed -Ei 's_(<prop oor:name="UseSystemFileDialog" oor:type="xs:boolean" oor:nillable="false"><value>)true(</value></prop>)_\1false\2_g' /usr/lib/libreoffice/share/registry/main.xcd
sed -Ei 's_(<prop oor:name="PickListSize" oor:type="xs:int" oor:nillable="false"><value>)25(</value></prop>)_\10\2_g' /usr/lib/libreoffice/share/registry/main.xcd
# sed -Ei 's_(<prop oor:name="AutoCheckEnabled" oor:type="xs:boolean" oor:op="replace"><value>)true(</value></prop>)_\1false\2_g' /usr/lib/libreoffice/share/registry/onlineupdate.xcd